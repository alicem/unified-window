public class SingleWindow.UnifiedWindow : Gtk.ApplicationWindow {
    private Gtk.EventBox content;

    public UnifiedWindow (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        get_style_context ().add_class ("unified");

        // Trick the window into being CSD
        set_titlebar (new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0));

        ensure_content ();
    }

    private void ensure_content () {
        if (content != null)
            return;

        content = new Gtk.EventBox ();
        content.expand = true;
        content.show ();
        base.add (content);
    }

    public override void add (Gtk.Widget widget) {
        if (widget is Gtk.Popover) {
            base.add (widget);
            return;
        }

        ensure_content ();

        content.add (widget);
    }

    public override void remove (Gtk.Widget widget) {
        if (widget == content || widget is Gtk.Popover) {
            base.remove (widget);
            return;
        }

        content.remove (widget);
    }

    public override void forall_internal (bool include_internal, Gtk.Callback callback) {
        if (include_internal)
            base.forall_internal (include_internal, callback);
        else if (content != null)
            content.forall_internal (include_internal, callback);
    }

    private Gtk.StyleContext get_child_context (string name) {
        var parent = get_style_context ();

        var path = parent.get_path ().copy ();
        var pos = path.append_type (typeof (Gtk.Widget));
        path.iter_set_object_name (pos, name);

        var context = new Gtk.StyleContext ();
        context.set_path (path);
        context.set_screen (parent.get_screen ());
        context.set_state (parent.get_state ());
        context.set_scale (parent.get_scale ());
        context.set_frame_clock (parent.get_frame_clock ());

        return context;
    }

    protected override bool draw (Cairo.Context cr) {
        var mask_corners = (decorated && !is_fullscreen () && !is_maximized);

        if (Gtk.cairo_should_draw_window (cr, get_window ())) {
            var width = get_allocated_width ();
            var height = get_allocated_height ();

            int x = 0;
            int y = 0;
            int w = width;
            int h = height;

            var context = get_child_context ("decoration");

            if (mask_corners) {
                var state = context.get_state ();
                var border = context.get_border (state);
                var padding = context.get_padding (state);
                border = sum_borders (border, padding);

                var shadow = get_shadow_width (context);

                x = shadow.left - border.left;
                y = shadow.top - border.top;
                w = width - (shadow.left + shadow.right - border.left - border.right);
                h = height - (shadow.top + shadow.bottom - border.top - border.bottom);
            }

            // GtkWindow adds this when it can't draw proper decorations, e.g. on a
            // non-composited WM on X11. This is documented, so we can rely on this
            // instead of copying the (pretty extensive) check.
            if (get_style_context ().has_class ("solid-csd")) {
                context.render_background (cr, 0, 0, width, height);
                context.render_frame (cr, 0, 0, width, height);
            } else {
                context.render_background (cr, x, y, w, h);
                context.render_frame (cr, x, y, w, h);
            }

            cr.save ();

            if (mask_corners)
                cr.push_group ();

            if (!get_app_paintable ()) {
                context = get_style_context ();
                context.render_background (cr, x, y, w, h);
                context.render_frame (cr, x, y, w, h);
            }

            propagate_draw (content, cr);

            context = get_child_context ("decoration-overlay");
            context.render_background (cr, x, y, w, h);
            context.render_frame (cr, x, y, w, h);

            if (mask_corners) {
                cr.pop_group_to_source ();
                cr.mask_surface (get_mask (w, h), x, y);
            }

            cr.restore ();
        }

        forall (child => {
            if (child == content || child == get_titlebar ())
                return;

            if (!child.visible || !child.get_child_visible ())
                return;

            var window = child.get_window ();
            if (child.get_has_window ())
                window = window.get_parent ();

            if (!Gtk.cairo_should_draw_window (cr, window))
                return;

            propagate_draw (child, cr);
        });

        return Gdk.EVENT_PROPAGATE;
    }

    private bool is_fullscreen () {
        return (get_window ().get_state () & Gdk.WindowState.FULLSCREEN) > 0;
    }

    private Gtk.Border sum_borders (Gtk.Border a, Gtk.Border b) {
        return {
            a.left + b.left,
            a.right + b.right,
            a.top + b.top,
            a.bottom + b.bottom,
        };
    }

    private Gtk.Border max_borders (Gtk.Border a, Gtk.Border b) {
        return {
            int16.max (a.left, b.left),
            int16.max (a.right, b.right),
            int16.max (a.top, b.top),
            int16.max (a.bottom, b.bottom),
        };
    }

    private Gtk.Border get_shadow_width (Gtk.StyleContext context) {
        if (!decorated)
            return {};

        if (is_maximized || is_fullscreen ())
            return {};

        if (!is_toplevel ())
            return {};

        var state = context.get_state ();

        var border = context.get_border (state);
        var padding = context.get_padding (state);
        var margin = context.get_margin (state);

        Gtk.Allocation alloc = {};
        Gtk.Allocation content_alloc = {};

        get_allocation (out alloc);
        content.get_allocation (out content_alloc);

        Gtk.Border shadow = {
            (int16) (content_alloc.x - alloc.x),
            (int16) (alloc.width - content_alloc.width - content_alloc.x),
            (int16) (content_alloc.y - alloc.y),
            (int16) (alloc.height - content_alloc.height - content_alloc.y),
        };

        border = sum_borders (padding, border);
        shadow = max_borders (shadow, margin);

        return sum_borders (border, shadow);
    }

    // TODO: this is very naive, pretty sure it can be done a lot more efficiently
    private Cairo.Surface get_mask (int w, int h) {
        var style = get_style_context ();
        var state = style.get_state ();

        var mask = new Cairo.ImageSurface (Cairo.Format.A8, w * scale_factor, h * scale_factor);

        double border_radius = (int) style.get_property (Gtk.STYLE_PROPERTY_BORDER_RADIUS, state);
        border_radius = border_radius.clamp (0, double.max (w / 2, h / 2));

        var cr = new Cairo.Context (mask);
        cr.set_source_rgb (0, 0, 0);
        rounded_rectangle (cr, 0, 0, w, h, border_radius);
        cr.fill ();

        return mask;
    }

    // FIXME: Need to support different radii for different corners.
    // There doesn't seem to be a way to get that from GTK though.
    private void rounded_rectangle (Cairo.Context cr, double x, double y, double width, double height, double radius) {
        const double ARC_0 = 0;
        const double ARC_1 = Math.PI * 0.5;
        const double ARC_2 = Math.PI;
        const double ARC_3 = Math.PI * 1.5;

        cr.new_sub_path ();
        cr.arc (x + width - radius, y + radius,          radius, ARC_3, ARC_0);
        cr.arc (x + width - radius, y + height - radius, radius, ARC_0, ARC_1);
        cr.arc (x + radius,         y + height - radius, radius, ARC_1, ARC_2);
        cr.arc (x + radius,         y + radius,          radius, ARC_2, ARC_3);
        cr.close_path ();
    }
}
