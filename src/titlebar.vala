public class SingleWindow.TitleBar : Gtk.EventBox {
    private Gtk.GestureMultiPress multipress_gesture;
    private Gtk.Menu? fallback_menu;
    private bool keep_above;

    static construct {
        set_css_name ("titlebar");
    }

    construct {
        multipress_gesture = new Gtk.GestureMultiPress (this);
        multipress_gesture.set_button (0);
        multipress_gesture.set_propagation_phase (Gtk.PropagationPhase.NONE);
        multipress_gesture.pressed.connect (pressed_cb);

        add_events (
            Gdk.EventMask.BUTTON_PRESS_MASK |
            Gdk.EventMask.BUTTON_RELEASE_MASK |
            Gdk.EventMask.BUTTON_MOTION_MASK |
            Gdk.EventMask.TOUCH_MASK
        );
    }

    protected override bool event (Gdk.Event event) {
        if (multipress_gesture == null)
            return Gdk.EVENT_PROPAGATE;

        var type = event.type;
        if (type != Gdk.EventType.BUTTON_PRESS &&
            type != Gdk.EventType.BUTTON_RELEASE &&
            type != Gdk.EventType.MOTION_NOTIFY &&
            type != Gdk.EventType.TOUCH_BEGIN &&
            type != Gdk.EventType.TOUCH_END &&
            type != Gdk.EventType.TOUCH_UPDATE)
            return Gdk.EVENT_PROPAGATE;

        var sequence = event.get_event_sequence ();
        var retval = multipress_gesture.handle_event (event);

        /* Reset immediately the gestures, here we don't get many guarantees
         * about whether the target window event mask will be complete enough
         * to keep gestures consistent, or whether any widget across the
         * hierarchy will be inconsistent about event handler return values.
         */
        if (multipress_gesture.get_sequence_state (sequence) == Gtk.EventSequenceState.DENIED)
            multipress_gesture.reset ();

        return retval;
    }

    private void pressed_cb (int n_press, double x, double y) {
        var sequence = multipress_gesture.get_current_sequence ();
        var button = multipress_gesture.get_current_button ();
        var event = multipress_gesture.get_last_event (sequence);

        if (event == null)
            return;

        if (get_display ().device_is_grabbed (multipress_gesture.get_device ()))
            return;

        switch (button) {
            case Gdk.BUTTON_PRIMARY:
                get_toplevel ().get_window ().raise ();

                if (n_press == 2)
                    titlebar_action (event, button, n_press);

                if (has_grab ())
                    multipress_gesture.set_sequence_state (sequence, Gtk.EventSequenceState.CLAIMED);

                break;

            case Gdk.BUTTON_SECONDARY:
                if (titlebar_action (event, button, n_press))
                    multipress_gesture.set_sequence_state (sequence, Gtk.EventSequenceState.CLAIMED);

                multipress_gesture.reset ();

                break;

            case Gdk.BUTTON_MIDDLE:
                if (titlebar_action (event, button, n_press))
                    multipress_gesture.set_sequence_state (sequence, Gtk.EventSequenceState.CLAIMED);

                break;
        }
    }

    private bool titlebar_action (Gdk.Event event, uint button, int n_press) {
        var settings = get_settings ();
        string? action = null;

        switch (button) {
            case Gdk.BUTTON_PRIMARY:
                if (n_press == 2)
                    action = settings.gtk_titlebar_double_click;
                break;

            case Gdk.BUTTON_MIDDLE:
                action = settings.gtk_titlebar_middle_click;
                break;

            case Gdk.BUTTON_SECONDARY:
                action = settings.gtk_titlebar_right_click;
                break;
        }

        if (action == null)
            return false;

        if (action == "none")
            return false;

        var toplevel = get_toplevel ();
        if (!(toplevel is Gtk.Window))
            return false;

        /* treat all maximization variants the same */
        if (action.has_prefix ("toggle-maximize")) {
            /*
             * gtk header bar won't show the maximize button if the following
             * properties are not met, apply the same to title bar actions for
             * consistency.
             */
            var toplevel_window = toplevel as Gtk.Window;
            if (toplevel_window.resizable &&
                toplevel_window.type_hint == Gdk.WindowTypeHint.NORMAL)
                toggle_maximized ();
            return true;
        }

        if (action == "lower") {
            toplevel.get_window ().lower ();
            return true;
        }

        if (action == "minimize") {
            toplevel.get_window ().iconify ();
            return true;
        }

        if (action == "menu") {
            do_popup (event);
            return true;
        }

        warning ("Unsupported titlebar action %s", action);
        return false;
    }

    private void toggle_maximized () {
        var toplevel = get_toplevel ();
        if (!(toplevel is Gtk.Window))
            return;

        var toplevel_window = toplevel as Gtk.Window;

        if (toplevel_window.is_maximized)
            toplevel_window.unmaximize ();
        else
            toplevel_window.maximize ();
    }

    private void do_popup (Gdk.Event event) {
        var toplevel = get_toplevel ();
        if (!(toplevel is Gtk.Window))
            return;

        if (toplevel.get_window ().show_window_menu (event))
            return;

        var toplevel_window = toplevel as Gtk.Window;

        if (fallback_menu != null)
            fallback_menu.destroy ();

        var state = toplevel.get_window ().get_state ();
        var iconified = (state & Gdk.WindowState.ICONIFIED) > 0;
        var maximized = toplevel_window.is_maximized && !iconified;

        fallback_menu = new Gtk.Menu ();
        fallback_menu.get_style_context ().add_class (Gtk.STYLE_CLASS_CONTEXT_MENU);

        fallback_menu.attach_to_widget (this, (widget) => {
            var self = widget as TitleBar;
            self.fallback_menu = null;
        });

        var menuitem = new Gtk.MenuItem.with_label (_("Restore"));
        menuitem.show ();

        /* "Restore" means "Unmaximize" or "Unminimize"
         * (yes, some WMs allow window menu to be shown for minimized windows).
         * Not restorable:
         *   - visible windows that are not maximized or minimized
         *   - non-resizable windows that are not minimized
         *   - non-normal windows
         */
        if ((toplevel_window.is_visible () && !(maximized || iconified)) ||
            (!iconified && !toplevel_window.resizable) ||
            toplevel_window.type_hint != Gdk.WindowTypeHint.NORMAL)
            menuitem.sensitive = false;

        menuitem.activate.connect (restore_window_clicked);
        fallback_menu.append (menuitem);

        menuitem = new Gtk.MenuItem.with_label (_("Move"));
        menuitem.show ();
        if (maximized || iconified)
            menuitem.sensitive = false;
        menuitem.activate.connect (move_window_clicked);
        fallback_menu.append (menuitem);

        menuitem = new Gtk.MenuItem.with_label (_("Resize"));
        menuitem.show ();
        if (!toplevel_window.resizable || maximized || iconified)
            menuitem.sensitive = false;
        menuitem.activate.connect (resize_window_clicked);
        fallback_menu.append (menuitem);

        menuitem = new Gtk.MenuItem.with_label (_("Minimize"));
        menuitem.show ();
        if (iconified || toplevel_window.type_hint != Gdk.WindowTypeHint.NORMAL)
            menuitem.sensitive = false;
        menuitem.activate.connect (minimize_window_clicked);
        fallback_menu.append (menuitem);

        menuitem = new Gtk.MenuItem.with_label (_("Maximize"));
        menuitem.show ();
        if (maximized || !toplevel_window.resizable ||
            toplevel_window.type_hint != Gdk.WindowTypeHint.NORMAL)
            menuitem.sensitive = false;
        menuitem.activate.connect (maximize_window_clicked);
        fallback_menu.append (menuitem);

        menuitem = new Gtk.SeparatorMenuItem ();
        menuitem.show ();
        fallback_menu.append (menuitem);

        menuitem = new Gtk.CheckMenuItem.with_label (_("Always on Top"));
        ((Gtk.CheckMenuItem) menuitem).active = keep_above;
        if (maximized)
            menuitem.sensitive = false;
        menuitem.show ();
        menuitem.activate.connect (ontop_window_clicked);
        fallback_menu.append (menuitem);

        menuitem = new Gtk.SeparatorMenuItem ();
        menuitem.show ();
        fallback_menu.append (menuitem);

        menuitem = new Gtk.MenuItem.with_label (_("Close"));
        menuitem.show ();
        if (!toplevel_window.deletable)
            menuitem.sensitive = false;
        menuitem.activate.connect (close_window_clicked);
        fallback_menu.append (menuitem);

        fallback_menu.popup_at_pointer (event);
    }

    private void restore_window_clicked () {
        var toplevel_window = get_toplevel () as Gtk.Window;
        if (toplevel_window == null)
            return;

        if (toplevel_window.is_maximized) {
            toplevel_window.unmaximize ();
            return;
        }

        var state = toplevel_window.get_window ().get_state ();
        if ((state & Gdk.WindowState.ICONIFIED) > 0)
            toplevel_window.deiconify ();
    }

    private void move_window_clicked () {
        var toplevel_window = get_toplevel () as Gtk.Window;
        if (toplevel_window == null)
            return;

        toplevel_window.begin_move_drag (0, 0, 0, Gdk.CURRENT_TIME);
    }

    private void resize_window_clicked () {
        var toplevel_window = get_toplevel () as Gtk.Window;
        if (toplevel_window == null)
            return;

        toplevel_window.begin_resize_drag (0, 0, 0, 0, Gdk.CURRENT_TIME);
    }

    private void minimize_window_clicked () {
        var toplevel_window = get_toplevel () as Gtk.Window;
        if (toplevel_window == null)
            return;

        /* Turns out, we can't iconify a maximized window */
        if (toplevel_window.is_maximized)
            toplevel_window.unmaximize ();

        toplevel_window.iconify ();
    }

    private void maximize_window_clicked () {
        var toplevel_window = get_toplevel () as Gtk.Window;
        if (toplevel_window == null)
            return;

        var state = toplevel_window.get_window ().get_state ();
        if ((state & Gdk.WindowState.ICONIFIED) > 0)
            toplevel_window.deiconify ();

        toplevel_window.maximize ();
    }

    private void ontop_window_clicked () {
        var toplevel_window = get_toplevel () as Gtk.Window;
        if (toplevel_window == null)
            return;

        // FIXME: It will go out of sync if something else calls
        // set_keep_above(), need to actually track it
        keep_above = !keep_above;
        toplevel_window.set_keep_above (keep_above);
    }

    private void close_window_clicked () {
        var toplevel_window = get_toplevel () as Gtk.Window;
        if (toplevel_window == null)
            return;

        toplevel_window.close ();
    }

    protected override void unrealize () {
        if (fallback_menu != null) {
            fallback_menu.destroy ();
            fallback_menu = null;
        }

        base.unrealize ();
    }
}
