[GtkTemplate (ui = "/org/example/Window/window.ui")]
public class SingleWindow.Window : UnifiedWindow {
    [GtkChild]
    private Gtk.Revealer revealer;
    [GtkChild]
    private Gtk.Image hide_btn_icon;
    [GtkChild]
    private Gtk.Stack light_dark_btn_stack;
    [GtkChild]
    private Gtk.Button light_dark_btn;

    public bool is_fullscreen { get; set; }
    public bool is_dark { get; set; }

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        notify["is-fullscreen"].connect (() => {
            if (is_fullscreen)
                fullscreen ();
            else
                unfullscreen ();
        });
        notify["is-dark"].connect (() => {
            Gtk.Settings.get_default ().gtk_application_prefer_dark_theme = is_dark;

            if (is_dark)
                get_style_context ().add_class ("dark");
            else
                get_style_context ().remove_class ("dark");

            light_dark_btn.tooltip_text = is_dark ? _("Light Mode") : _("Dark Mode");
            light_dark_btn_stack.visible_child_name = is_dark ? "light" : "dark";
        });
    }

    [GtkCallback]
    private void hide_btn_clicked_cb () {
        revealer.reveal_child = !revealer.reveal_child;
        var dir = revealer.reveal_child ? "up" : "down";
        hide_btn_icon.icon_name = @"go-$dir-symbolic";
    }

    [GtkCallback]
    private void fullscreen_clicked_cb () {
        is_fullscreen = true;
    }

    [GtkCallback]
    private void restore_clicked_cb () {
        is_fullscreen = false;
    }

    [GtkCallback]
    private void light_dark_clicked_cb () {
        is_dark = !is_dark;
    }

}
